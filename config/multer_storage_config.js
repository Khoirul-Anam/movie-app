const multer = require('multer');
const fs = require('fs');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      if (fs.existsSync('public/img/')) {
        cb(null, 'public/img')
      } else {
        fs.mkdirSync('public/img', {recursive: true})
        cb(null, 'public/img')
      }
    },
    filename: function (req, file, cb) {
      const uniqueSuffix = Date.now() + '-' + file.originalname
      cb(null, file.fieldname + '-' + uniqueSuffix)
    }
  })
  
  module.exports = storage