const adminUserRouter = require("./cms/admin_user_routes");
const cmsRoutes = require("./cms/cms_routes");
const movie_router = require("./app/movie_routes");
const userRouter = require("./app/user_routes");
const wishlistRouter = require("./app/wishlist_routes");
const router = require("express").Router();

router.use("/", userRouter);
router.use("/", movie_router);
router.use("/", wishlistRouter);
router.use("/cms/", cmsRoutes);
router.use("/", adminUserRouter);

module.exports = router;
