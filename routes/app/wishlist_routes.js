const WishlistController = require("../../controllers/app/wishlish_controller");
const ErrorResponse = require("../../error_messages");
const jwt = require('jsonwebtoken');
const wishlistRouter = require("express").Router();

wishlistRouter.post(
  "/wishlist",
  (req, _res, next) => {
    try {
      const { authorization } = req.headers;
      if (authorization === null || authorization === undefined)
        throw ErrorResponse.unAuth;
      jwt.verify(authorization, process.env.JWT_SECRET, (err, decodedToken) => {
        if (err) {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
        req.user = decodedToken;
        next();
      });
    } catch (error) {
      next(error);
    }
  },
  WishlistController.addWishlist
);
wishlistRouter.get(
  "/wishlist",
  (req, _res, next) => {
    try {
      const { authorization } = req.headers;
      console.log(authorization);
      if (authorization === null || authorization === undefined)
        throw ErrorResponse.unAuth;
        console.log(process.env.JWT_SECRET);
      jwt.verify(authorization, process.env.JWT_SECRET, (err, decodedToken) => {
        if (err) {
          console.log(err);
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
        req.user = decodedToken;
        console.log(req.user);
        next();
      });
    } catch (error) {
      next(error);
    }
  },
  WishlistController.getWishlist
);
wishlistRouter.delete(
  "/wishlist/:id",
  (req, _res, next) => {
    try {
      const { authorization } = req.headers;
      if (authorization === null || authorization === undefined)
        throw ErrorResponse.unAuth;
      jwt.verify(authorization, process.env.JWT_SECRET, (err, decodedToken) => {
        if (err) {
          throw {
            status: 401,
            message: "Unauthorized request",
          };
        }
        req.user = decodedToken;
        next();
      });
    } catch (error) {
      next(error);
    }
  },
  WishlistController.deleteWishlist
);

module.exports = wishlistRouter;
