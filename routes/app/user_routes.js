const UserController = require("../../controllers/app/user_controller");

const userRouter = require("express").Router();
const multer = require("multer");
const storage = require("../../config/multer_storage_config");
const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/jpg" ||
      file.mimetype === "image/png"
    ) {
      cb(null, true);
    } else {
      cb(new Error("File should be an image"), false);
    }
  },
  limits: {
    fileSize: 3000000,
  },
});

userRouter.post("/login", UserController.login);
userRouter.post(
  "/sign-up",
  upload.single("profile_img"),
  UserController.regUser
);
userRouter.post("/forgot-pass-token", UserController.sendForgotPassToken);
userRouter.post("/forgot-pass-verify", UserController.verifyForgotPassToken);
userRouter.post("/change-pass", UserController.changePassword);

module.exports = userRouter;
