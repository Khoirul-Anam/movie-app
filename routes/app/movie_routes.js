const MovieController = require('../../controllers/app/movie_controller');

const movie_router = require('express').Router();

movie_router.get('/movies', MovieController.getAllMovies);

module.exports = movie_router;