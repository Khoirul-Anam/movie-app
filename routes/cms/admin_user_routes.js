const AdminController = require('../../controllers/cms/admin_controller');
const authenticateToken = require('../../services/token_auth_service')
const adminUserRouter = require('express').Router();

adminUserRouter.post('/admin-login', AdminController.loginAdmin);
adminUserRouter.post('/new-admin', authenticateToken, AdminController.createAdmin);

module.exports = adminUserRouter;