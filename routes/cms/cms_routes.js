const CMSController = require("../../controllers/cms/cms_controller");
const authenticateToken = require("../../services/token_auth_service");

const cmsRoutes = require("express").Router();
const multer = require("multer");
const storage = require("../../config/multer_storage_config");
const upload = multer({
  storage: storage,
  fileFilter: (req, file, cb) => {
    if (
      file.mimetype === "image/jpeg" ||
      file.mimetype === "image/jpg" ||
      file.mimetype === "image/png"
    ) {
      cb(null, true);
    } else {
      cb(new Error("File should be an image"), false);
    }
  },
  limits: {
    fileSize: 3000000
  }
});

// TODO: add authentikasi
cmsRoutes.get("/movies", authenticateToken, CMSController.getMovieList);
cmsRoutes.post("/movies", upload.single('image'), authenticateToken, CMSController.addNewMovie);
cmsRoutes.post("/public-message", authenticateToken, CMSController.sendMovieInfo)
cmsRoutes.put("/movies/:id", authenticateToken, upload.single('image'), CMSController.updateMovie);
cmsRoutes.delete("/movies/:id", authenticateToken, CMSController.deleteMovie);

module.exports = cmsRoutes;
