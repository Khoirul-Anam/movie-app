const { Admin, User } = require('../models');
const bcrypt = require('bcryptjs');
const ErrorResponse = require('../error_messages');

const findAdmin = async (email, password) => {
  if (!email || !password) throw ErrorResponse.invalidLogin;
  const admin = await Admin.findOne({
    where: {
      email,
    },
  });
  if (admin === null) throw ErrorResponse.invalidLogin;
  if (!bcrypt.compareSync(password, admin.password))
    throw ErrorResponse.invalidLogin;

    return admin;
};

module.exports = findAdmin
