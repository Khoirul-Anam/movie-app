const jwt = require("jsonwebtoken");
const ErrorResponse = require("../error_messages");

module.exports = async (req, _res, next) => {
  try {
    const { authorization } = req.headers;
    console.log(authorization);
    if (authorization === null || authorization === undefined)
      throw ErrorResponse.unAuth;
    jwt.verify(authorization, process.env.JWT_SECRET, (err, decodedToken) => {
      if (err) {
        console.log(err);
        throw ErrorResponse.unAuth;
      }
      req.user = decodedToken;
      
      console.log(req.user);
      next();
    });
  } catch (error) {
    next(error);
  }
};
