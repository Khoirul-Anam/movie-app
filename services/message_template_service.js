class MessageTemplate {
  static getRegMsg(user) {
    return {
      subject: "Berhasil Registrasi",
      message: `Registrasi berhasil... \n Selamat datang ${user}..`,
    };
  }

  static getFrgPassMsg(token, date) {
    return {
      subject: "Kode Verifikasi Lupa Password",
      message: `Kode verifikasi token adalah ${token} \n Email dibuat pada ${date}`,
    };
  }

  static getMovieMsg(movieName) {
    return {
      subject: `Film ${movieName} sudah tayang!!`,
      message: `Film ${movieName} sudah tayang di seluruh bioskop di Indonesia`,
    };
  }
}

module.exports = MessageTemplate
