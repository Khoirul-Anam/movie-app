const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const { OAuth2Client } = require("google-auth-library");
const { User } = require('../models')
const client = new OAuth2Client(
  process.env.GOOGLE_CLIENT_ID,
  process.env.GOOGLE_CLIENT_SECRET
);
const axios = require("axios");
const MessageTemplate = require("./message_template_service");
const sendEmail = require('./send_email_service')

class LoginMethod {
    static googleLogin = async (google_id_token) => {
      console.log(process.env.GOOGLE_CLIENT_ID);
      const payload = await client.verifyIdToken({
        idToken: google_id_token,
        requiredAudience: process.env.GOOGLE_CLIENT_ID,
      });
      console.log(payload);
      const user = await User.findOne({
        where: {
          email: payload.payload.email,
        },
      });
      if (!user) {
        const createdUser = await User.create({
          email: payload.payload.email,
          password: '',
        });
        const messageContent = MessageTemplate.getRegMsg(payload.payload.email);
        const token = jwt.sign(
          {
            id: createdUser.id,
            email: createdUser.email,
          },
          process.env.JWT_SECRET
        );
        await sendEmail(
            process.env.ADMIN_EMAIL,
            payload.payload.email,
            null,
            messageContent.message,
            messageContent.subject
          );
        return token
      }
      const token = jwt.sign(
        {
          id: user.id,
          email: user.email,
        },
        process.env.JWT_SECRET
      );
      return token
    };
  
    static facebookLogin = async (facebook_id_token) => {
      const response = await axios.get(
        `https://graph.facebook.com/v12.0/me?fields=id%2Cname%2Cemail%2Cgender%2Cbirthday&access_token=${facebook_id_token}`
      );
      console.log(response);
      const user = await User.findOne({
        where: {
          email: response.data.email,
        },
      });
      if (!user) {
        const createdUser = await User.create({
          email: response.data.email,
          password: '',
        });
        const messageContent = MessageTemplate.getRegMsg(response.data.email);
        const token = jwt.sign(
          {
            id: createdUser.id,
            email: createdUser.email,
          },
          process.env.JWT_SECRET
        );
        await sendEmail(
            process.env.ADMIN_EMAIL,
            response.data.email,
            null,
            messageContent.message,
            messageContent.subject
          );
        return token
      }
      const token = jwt.sign(
        {
          id: user.id,
          email: user.email,
        },
        process.env.JWT_SECRET
      );
      return token;
    };
  
    static manualLogin = async (email, password) => {
      const user = await User.findOne({
        where: {
          email,
        },
      });
      if (!bcrypt.compareSync(password, user.password))
        throw ErrorResponse.invalidLogin;
      const token = jwt.sign(
        {
          id: user.id,
          email: user.email,
        },
        process.env.JWT_SECRET
      );
      console.log(`secret key: ${process.env.JWT_SECRET}`);
      return token
    };
  }

  module.exports = LoginMethod