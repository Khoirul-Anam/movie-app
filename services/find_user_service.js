const { User } = require('../models');
const ErrorResponse = require('../error_messages');

const checkUserExist = async (email) => {
  const user = await User.findOne({
    where: {
      email,
    },
  });
  if (user !== null) throw ErrorResponse.unAuth;
    return user;
};

module.exports = checkUserExist
