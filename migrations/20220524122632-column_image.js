'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
          queryInterface.addColumn('Movies', 'image', {
              type: Sequelize.STRING
          }, { transaction: t }),
          queryInterface.addColumn('Users', 'profile_img', {
              type: Sequelize.STRING,
          }, { transaction: t })
      ])
  })
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction((t) => {
      return Promise.all([
          queryInterface.removeColumn('Movies', 'image', { transaction: t }),
          queryInterface.removeColumn('Users', 'profile_img', { transaction: t })
      ])
  })
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
  }
};
