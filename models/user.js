const bcrypt = require('bcryptjs');

'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      
    }
  }
  User.init({
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    profile_img: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'User',
  });


  User.addHook('beforeCreate', (user, options)=>{
    const salt = bcrypt.genSaltSync(10);
    user.password = bcrypt.hashSync(user.password, salt);
  })
  return User;
};