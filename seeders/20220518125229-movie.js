'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Movies', [
      {
        name: 'Star Wars: The Force Awaken',
        synopsis: 'Do voluptate do cupidatat aliqua magna do eiusmod. Occaecat ea est ullamco consectetur. Velit nulla et Lorem nulla do deserunt quis aliqua nisi laboris exercitation laboris aute. Commodo sit fugiat laboris est.',
        duration: Math.floor(Math.random() * 90) + 40,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Hachiko: A Dog\'s Story',
        synopsis: 'Duis proident mollit exercitation adipisicing amet Lorem minim ipsum commodo fugiat voluptate et. Eiusmod exercitation et anim ex sunt. Non reprehenderit amet cillum dolore adipisicing sit veniam ea quis laboris tempor.',
        duration: Math.floor(Math.random() * 90) + 40,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'Narnia: The Chronicle of Prince Caspian',
        synopsis: 'Eiusmod anim nulla cillum culpa duis in sint sunt ut id est laborum. Consequat minim consectetur ad pariatur. Sint sint in velit sit exercitation anim ullamco eiusmod cillum. Esse eu eu sit qui dolor eiusmod quis.',
        duration: Math.floor(Math.random() * 90) + 40,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: 'The Great Wall',
        synopsis: 'Incididunt ullamco culpa qui aute do sint incididunt culpa sit ut exercitation. Commodo mollit adipisicing ullamco labore aliquip ullamco officia irure est. Consequat aute esse amet ipsum laboris anim sunt adipisicing proident laborum dolor tempor labore reprehenderit. Adipisicing et labore incididunt non minim adipisicing et sint. Mollit eu ullamco veniam et culpa eu quis sit sint laborum irure dolor nisi. Pariatur ea in proident amet anim sunt occaecat et dolor culpa. Pariatur sunt sunt id anim commodo excepteur cillum ipsum ad nulla sit veniam pariatur excepteur.',
        duration: Math.floor(Math.random() * 90) + 40,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Movies', {}, { truncate: true, restartIdentity: true })
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
