const bcrypt = require('bcryptjs');
const { User } = require('../models');


'use strict';


module.exports = {
  async up (queryInterface, Sequelize) {
    const salt = bcrypt.genSaltSync(10);
    const hashedPw = bcrypt.hashSync('admin', salt);

    await queryInterface.bulkInsert('Users', [
      {
        email: 'anammail@rmail.com',
        password: hashedPw,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        email: 'anam@mail.com',
        password: hashedPw,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        email: 'budimail@mail.com',
        password: hashedPw,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        email: 'hayo@gmail.com',
        password: hashedPw,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', {}, { truncate: true, restartIdentity: true })
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
