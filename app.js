const express = require("express");
const app = express();
const port = 3000;
const routes = require("./routes");
const errorHandler = require("./error_handler");
const Sentry = require("@sentry/node");
const morgan = require("morgan");
const path = require("path");
require("dotenv").config({ path: "./.env" });

Sentry.init({
  dsn: process.env.SENTRY_DSN,
});
app.use((req, res, next) => {
  req.sentry = Sentry;
  next();
});
app.use(express.static(path.join('public', 'img')))
// morgan
app.use(morgan("tiny"));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(routes);
app.use(errorHandler);

// app.listen(port, () => {
//   console.log(`Example app listening on port ${port}`)
// })

module.exports = app;
