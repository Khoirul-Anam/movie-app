const ErrorResponse = require("../../error_messages");
const { User } = require("../../models");
const LoginMethod = require("../../services/login_services");
const MessageTemplate = require("../../services/message_template_service");
const sendEmail = require("../../services/send_email_service");
const otpGenerator = require("otp-generator");

class UserController {
  static async regUser(req, res, next) {
    try {
      const { email, password } = req.body;
      if (!email || !password || !req.file) throw ErrorResponse.badReq;
      const user = await User.create({
        email,
        password,
        profile_img: req.file.path,
      });
      const messageContent = MessageTemplate.getRegMsg(user.email);
      res.status(201).json({
        message: "Success menambah user",
      });
      await sendEmail(
        process.env.ADMIN_EMAIL,
        user.email,
        null,
        messageContent.message,
        messageContent.subject
      );
    } catch (error) {
      next(error);
    }
  }

  static async login(req, res, next) {
    try {
      let token;
      const { email, password, google_id_token, facebook_id_token } = req.body;
      console.log(google_id_token);
      if (google_id_token) {
        token = await LoginMethod.googleLogin(google_id_token);
        return res.status(200).json({
          your_token: token,
        });
      }
      if (facebook_id_token) {
        token = await LoginMethod.facebookLogin(facebook_id_token);
        return res.status(200).json({
          your_token: token,
        });
      }
      if (!email || !password) throw ErrorResponse.invalidLogin;
      if (email && password) {
        token = await LoginMethod.manualLogin(email, password);
        return res.status(200).json({
          your_token: token,
        });
      }
    } catch (error) {
      console.log(error);
      next(error);
    }
  }
  static async sendForgotPassToken(req, res, next) {
    try {
      const { email } = req.body;
      const user = await User.findOne({
        where: {
          email,
        },
      });
      if (!user) throw ErrorResponse.invalidEmail;

      const otp = otpGenerator.generate(6, {
        upperCaseAlphabets: false,
        specialChars: false,
      });
      const salt = bcrypt.genSaltSync(10);
      const hash = bcrypt.hashSync(otp, salt);
      await User.update(
        {
          forgot_pass_token: hash,
          forgot_pass_token_expired_at: new Date(
            new Date().getTime() + 5 * 60000
          ),
        },
        {
          where: {
            email: req.body.email,
          },
        }
      );
      const messageContent = MessageTemplate.getFrgPassMsg(token, new Date());
      await sendEmail(
        process.env.ADMIN_EMAIL,
        email,
        null,
        messageContent.message,
        messageContent.subject
      );
      res.status(200).json({
        message: "Succesfully send email",
      });
    } catch (error) {
      next(error);
    }
  }

  static async verifyForgotPassToken(req, res, next) {
    try {
      const { email } = req.body;
      const user = await User.findOne({
        where: {
          email,
        },
      });
      if (!bcrypt.compareSync(req.body.token, user.forgot_pass_token))
        throw ErrorResponse.sendCustomResponse(400, "Invalid Token");
      if (user.forgot_pass_token_expired_at < new Date())
        throw ErrorResponse.sendCustomResponse(400, "Invalid Token");
      res.status(200).json({
        valid: true,
        message: "Token is valid",
      });
    } catch (error) {
      next(error);
    }
  }

  static async changePassword(req, res, next) {
    const { email, token, password, password_confirmation } = req.body;
    if (password !== password_confirmation)
      throw ErrorResponse.sendCustomResponse(
        400,
        "Password does not match password confirmation"
      );
    const user = await User.findOne({
      where: {
        email,
      },
    });
    if (!user)
      throw ErrorResponse.sendCustomResponse(400, "Invalid user or token");
    if (!bcrypt.compareSync(token, user.forgot_pass_token))
      throw ErrorResponse.sendCustomResponse(400, "Invalid user or token");
    if (user.forgot_pass_token_expired_at < new Date())
      throw ErrorResponse.sendCustomResponse(400, "Invalid user or token");

    const salt = bcrypt.genSaltSync(10);
    const hash = bcrypt.hashSync(password, salt);
    await User.update(
      {
        password: hash,
        forgot_pass_token: null,
        forgot_pass_token_expired_at: null,
      },
      {
        where: {
          email,
        },
      }
    );
    res.status(200).json({
      message: "Successfully change password",
    });
  }
}

module.exports = UserController;
