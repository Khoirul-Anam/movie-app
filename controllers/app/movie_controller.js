const { Movie } = require("../../models");

class MovieController {
  static async getAllMovies(req, res, next) {
    try {
      const movies = await Movie.findAll();
      res.status(200).json(movies);
    } catch (error) {
      next(error);
    }
  }
}

module.exports = MovieController;
