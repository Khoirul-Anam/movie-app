const sequelize = require("sequelize");
const { Op } = sequelize;
const ErrorResponse = require("../../error_messages");
const { Wishlist, Movie } = require("../../models");

class WishlistController {
  static async addWishlist(req, res, next) {
    try {
      const { movie_id } = req.body;
      const movie = await Movie.findOne({
        where: {
          id: movie_id,
        },
      });
      if (movie === null) throw ErrorResponse.notFound;

      await Wishlist.create({
        user_id: req.user.id,
        movie_id,
      });
      res.status(200).json({
        message: "success added wishlist",
      });
    } catch (error) {
      next(error);
    }
  }

  static async getWishlist(req, res, next) {
    try {
      const wishlists = await Wishlist.findAll({
        where: {
          user_id: req.user.id,
        },
        include: {
          model: Movie,
          as: "movie",
          attributes: [],
        },
        attributes: [
          [sequelize.literal('"movie"."name"'), "movie_name"],
          [sequelize.literal('"movie"."synopsis"'), "movie_synopsis"],
          [sequelize.literal('"movie"."duration"'), "duration"],
        ],
      });
      res.status(200).json(wishlists);
    } catch (error) {
      next(error);
    }
  }

  static async deleteWishlist(req, res, next) {
    try {
      const { id } = req.params;
      const wishlist = await Wishlist.findAll({
        where: {
          [Op.and]: [{ user_id: req.user.id }, { movie_id: id }],
        },
      });
      console.log(wishlist);
      if (wishlist.length === 0) throw ErrorResponse.notFound;
      await Wishlist.destroy({
        where: {
          movie_id: id,
        },
      });
      res.status(200).json({
        message: "success delete movie from wishlist",
      });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = WishlistController;
