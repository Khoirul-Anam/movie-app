const checkUserExist = require("../../services/find_user_service");
const { Movie, User } = require("../../models");
const ErrorResponse = require("../../error_messages");
const sendEmail = require('../../services/send_email_service');
const MessageTemplate = require("../../services/message_template_service");
class CMSController {
  static async getMovieList(req, res, next) {
    try {
      await checkUserExist(req.user.email);
      const movies = await Movie.findAll();
      res.status(200).json(movies);
    } catch (error) {
      next(error);
    }
  }

  static async addNewMovie(req, res, next) {
    try {
      await checkUserExist(req.user.email);
      const { name, synopsis, duration } = req.body;
      if (!name || !synopsis || !duration) throw ErrorResponse.badReq;
      console.log(req.file.path);
      await Movie.create({
        name,
        synopsis,
        duration,
        image: req.file.path,
      });
      res.status(201).json({
        message: "Success add new movie",
      });
    } catch (error) {
      next(error);
    }
  }

  static async updateMovie(req, res, next) {
    try {
      await checkUserExist(req.user.email);
      const { id } = req.params;
      const { name, synopsis, duration } = req.body;
      if (!name && !synopsis && !duration && !req.file)
        throw ErrorResponse.badReq;

      const movie = await Movie.findOne({
        where: {
          id,
        },
      });
      if (movie === null) throw ErrorResponse.notFound;
      await Movie.update(
        { 
          ...req.body, 
          image: req.file.path },
        {
          where: {
            id,
          },
        }
      );
      res.status(200).json({
        message: "Success update movie",
      });
    } catch (error) {
      next(error);
    }
  }

  static async deleteMovie(req, res, next) {
    try {
      await checkUserExist(req.user.email);
      const { id } = req.params;
      const movie = await Movie.findOne({
        where: {
          id,
        },
      });
      if (movie === null) throw ErrorResponse.notFound;
      await Movie.destroy({
        where: {
          id,
        },
      });
      res.status(200).json({
        message: "Success delete movie",
      });
    } catch (error) {
      next(error);
    }
  }

  static async sendMovieInfo(req, res, next){
    try {
      const { userIds, subject, text } = req.body;
      let emailList = [];
      await Promise.all(userIds.map(async (userId)=>{
        const user = await User.findOne({
          where: {
            id: userId
          }
        });
        if(!user) throw ErrorResponse.sendCustomResponse(400, 'One user does not exist')
        emailList.push(user.email);
      }))
      await Promise.all(emailList.map(async (destEmail)=>{
        await sendEmail(process.env.ADMIN_EMAIL, destEmail, null, text, subject)
      }))
      res.status(200).json({
        message: 'All emails has been sent'
      })
    } catch (error) {
      next(error)
    }
  }
}

module.exports = CMSController;
