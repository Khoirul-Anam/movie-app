const findAdmin = require("../../services/find_admin_service");
const jwt = require("jsonwebtoken");
const { Admin } = require("../../models");
const ErrorResponse = require("../../error_messages");
const checkUserExist = require("../../services/find_user_service");

class AdminController {
  static async loginAdmin(req, res, next) {
    try {
      const { email, password } = req.body;
      const admin = await findAdmin(email, password);
      const token = jwt.sign(
        { id: admin.id, email: admin.email, role: admin.role },
        process.env.JWT_SECRET
      );
      res.status(200).json({ token });
    } catch (error) {
      next(error);
    }
  }

  static async createAdmin(req, res, next) {
    try {
      if (req.user.role !== "super") throw ErrorResponse.unAuth;
      const { email, password, role } = req.body;
      if (!email || !password || !role) throw ErrorResponse.badReq;
      await checkUserExist(email);
      await Admin.create({
        email,
        password,
        role,
      });
      res.status(201).json({
        message: "Success added new admin",
      });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = AdminController;
