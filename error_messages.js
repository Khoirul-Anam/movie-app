class ErrorResponse {
  static badReq = {
    status: 400,
    message: "Bad request",
  };
  static invalidEmail = {
    status: 400,
    message: "Invalid Email",
  };

  static notFound = {
    status: 404,
    message: "Not found",
  };

  static unAuth = {
    status: 401,
    message: "Unauthorized request",
  };

  static invalidLogin = {
    status: 400,
    message: "Invalid Email or Password",
  };

  static sendCustomResponse = (status, message) => ({
    status,
    message,
  });
}

module.exports = ErrorResponse;
